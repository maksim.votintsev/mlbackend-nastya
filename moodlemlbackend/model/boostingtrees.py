"""XGBoost classifier"""

from sklearn import datasets
from sklearn.model_selection import train_test_split
import xgboost as xgb
import numpy as np
from sklearn.metrics import precision_score


class XGBoost(object):
    """XGBoost classifier"""

    def __init__(self, n_features, n_classes, n_epoch, batch_size,
                 starter_learning_rate, tensor_logdir, initial_weights=False):

        self.n_epoch = n_epoch
        self.batch_size = batch_size
        self.starter_learning_rate = starter_learning_rate
        self.n_features = n_features

        # Based on the number of features although we need a reasonable
        # minimum.
        # self.n_hidden = max(4, int(n_features / 3))
        self.n_classes = n_classes
        self.tensor_logdir = tensor_logdir

        self.x = None
        self.y_ = None
        self.y = None
        self.probs = None
        self.loss = None

        # self.build_graph(initial_weights)

        self.start_session()

        # During evaluation we process the same dataset multiple times,
        # could could store each run result to the user but results would
        # be very similar we only do it once making it simplier to
        # understand and saving disk space.
        # if os.listdir(self.tensor_logdir) == []:
        #     self.log_run = True
        #     self.init_logging()
        # else:
        #     self.log_run = False

    def __getstate__(self):
        state = self.__dict__.copy()
        # del state['x']
        # del state['y_']
        # del state['y']
        # del state['probs']
        del state['sess']

        # We also remove this as it depends on the run.
        # del state['tensor_logdir']

        return state

    def __setstate__(self, state):
        self.__dict__.update(state)
        self.build_graph()
        self.start_session()

    def set_tensor_logdir(self, tensor_logdir):
        """Sets tensorflow logs directory

        Needs to be set separately as it depends on the
        run, it can not be restored."""
        pass

        # self.tensor_logdir = tensor_logdir
        # try:
        #     self.file_writer
        #     self.merged
        # except AttributeError:
        #     # Init logging if logging vars are not defined.
        #     self.init_logging()

    def build_graph(self, initial_weights=False):
        """Builds the computational graph without feeding any data in"""
        # apendics from Tensor
        pass

    def start_session(self):
        """Starts the session"""
        # apendics from Tensor
        self.sess = xgb.Booster({'nthread': 4})  # init model

    def init_logging(self):
        """Starts logging the tensors state"""
        # apendics from Tensor
        pass

    def get_session(self):
        """Return the session"""
        return self.sess

    def get_n_features(self):
        """Return the number of features"""
        return self.n_features

    def get_n_classes(self):
        """Return the number of features"""
        return self.n_classes

    def fit(self, X, y):
        """Fits provided data into the session"""
        dtrain = xgb.DMatrix(X, label=y)
        param = {
                    'max_depth': 3,
                    'eta': 0.3, 
                    #    'silent': 1,
                    'objective': 'multi:softprob',
                    'num_class': self.n_classes
                }
        num_round = 20
        self.sess = xgb.train(param, dtrain, num_round)
        print('Что-то да отработало?')
        preds_2_dlya_nasty_trl = self.sess.predict( xgb.DMatrix( np.array([[5, 4, 7]]), label=np.array([[0]])) )
        print('\n Predictionsis: ')
        print(preds_2_dlya_nasty_trl)

    def predict_proba(self, x, y):
        """Returns predicted probabilities"""
        x_size = x.shape
        # print(X_test_size)
        # y = [[]] * x_size[0]
        # y[0].append(0)
        y = np.array(y)
        # print(np.array(y))
        dtest = xgb.DMatrix(x, label=y)
        return self.sess.predict(dtest)

    def predict(self, x, y):
        """Returns predictions"""
        probs = self.predict_proba(x, y) # very stupid, but work
        return np.argmax(probs, axis = 1) # prediction is index of MAX probs's element by another one raw

