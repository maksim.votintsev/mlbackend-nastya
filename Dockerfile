FROM tiangolo/meinheld-gunicorn-flask:python3.8

COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt

COPY app app
COPY app/webapp.py app/main.py
